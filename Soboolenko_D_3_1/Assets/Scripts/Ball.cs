﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public Vector3 Direction = Vector3.down;
    public bool IsPlayerControll;
    [SerializeField]
    private float Speed;
    void Update()
    {
        if (IsPlayerControll == false)
        {
            transform.position += Direction * Speed * Time.deltaTime;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Direction = Vector3.Reflect(Direction, collision.GetContact(0).normal);
    }
}
