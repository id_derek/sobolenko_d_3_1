﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;
    [HideInInspector]
    public LevelConfiguration[] LevelConfigs;
    public void Start()
    {
        LevelConfigs = Resources.LoadAll<LevelConfiguration>("");
        if (instance == null)
        {
            instance = this;
        }
    }
}
