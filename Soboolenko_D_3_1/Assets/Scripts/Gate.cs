﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    public Player Player1;
    public Player Player2;
    public GameObject[] Walls;
    private void OnCollisionEnter(Collision collision)
    {
        foreach (var item in Walls)
        {
            if (collision.gameObject == item) {
                return;
            }
        }
        var ball = collision.gameObject.GetComponent<Ball>();
        if (ball != null)
        {
            Player1.SetBallControll(ball, true);
        }
    }
}
