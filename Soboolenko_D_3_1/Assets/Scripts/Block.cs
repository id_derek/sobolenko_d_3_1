﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    [SerializeField]
    private int _index;
    void Start()
    {
        if (Player.LevelIndex == _index)
        {
            Player.Blocks.Add(this);
        }
        MeshRenderer renderer = GetComponent<MeshRenderer>();
        transform.rotation = Quaternion.Euler(new Vector3(Random.Range(-15,15), Random.Range(-15, 15), Random.Range(-15, 15)));
        renderer.materials[0].SetColor("_EmissionColor", RandomColor());
        renderer.materials[0].SetColor(0, RandomColor());
    }

    private Color RandomColor()
    {
        return new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
    }
}
