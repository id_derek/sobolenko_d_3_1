﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelConfiguration : ScriptableObject
{
    public int index;
    public List<Block> Blocks = new List<Block>();
    public Vector3 Position;
    public Transform Transform;

    private void OnEnable()
    {
        if (Position == null && Transform != null)
        {
            Position = Transform.position;
        }
    }
}
