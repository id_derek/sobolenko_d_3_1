﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Player : MonoBehaviour
{
    public enum PlayerType
    {
        Player1 = 0, Player2 = 1
    }
    private Rigidbody _rb;
    public static List<Block> Blocks = new List<Block>();
    public static int LevelIndex = 0;
    public PlayerType Type;
    [SerializeField]
    private float _speed;
    private Vector3 _velocity;
    public Vector3 MaxVelocity;
    [SerializeField]
    private float _inertiaTime;
    private void Start()
    {
        _rb = GetComponent<Rigidbody>();

        _rb.velocity = Vector3.ClampMagnitude(_velocity, MaxVelocity.magnitude);
    }
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            SetBallControll(false);
        }

        if (Type == PlayerType.Player1)
        {
            if (Input.GetKey(KeyCode.A))
            {

                _velocity += new Vector3(0, 0, _speed);
            }
            if (Input.GetKey(KeyCode.D))
            {

                _velocity -= new Vector3(0, 0, _speed);
            }
            if (Input.GetKey(KeyCode.W))
            {

                _velocity += new Vector3(_speed, 0, 0);
            }
            if (Input.GetKey(KeyCode.S))
            {
                _velocity -= new Vector3(_speed, 0, 0);
            }
        }

        if (Type == PlayerType.Player2)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {

                _velocity += new Vector3(0, 0, _speed);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {

                _velocity -= new Vector3(0, 0, _speed);
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {

                _velocity += new Vector3(_speed, 0, 0);
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {

                _velocity -= new Vector3(_speed, 0, 0);
            }
        }


        _rb.velocity = new Vector3(_velocity.x, _velocity.y, _velocity.z) * Time.deltaTime * _speed;
    }

    public void SetBallControll(bool isControl)
    {
        SetBallControll(FindObjectOfType<Ball>(), isControl);
    }
    public void SetBallControll(Ball ball, bool isControl)
    {
        if (isControl == true)
        {
            ball.IsPlayerControll = true;
            ball.transform.position = transform.position;
            ball.transform.parent = transform;
        }
        else
        {
            ball.IsPlayerControll = false;
            ball.transform.parent = null;
        }
    }
}
